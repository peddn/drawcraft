let path = require('path');
let express = require('express');

let drawcraft = express();


drawcraft.use('/js', express.static(path.join(__dirname, '../client/js')));
drawcraft.use('/css', express.static(path.join(__dirname, '../client/css')));
drawcraft.use('/assets', express.static(path.join(__dirname, '../client/assets')));

drawcraft.use('/lib/pixijs', express.static(path.join(__dirname, '../../node_modules/pixi.js/dist')));
drawcraft.use('/lib/howler', express.static(path.join(__dirname, '../../node_modules/howler/dist')));

drawcraft.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/index.html'));
});

drawcraft.listen(3000, function () {
  console.log('drawcraft server listening on port 3000');
});
