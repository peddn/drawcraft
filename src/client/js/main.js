import DrawcraftGame from './DrawcraftGame.js';

let pixiConfig = { 
    width: 1024, 
    height: 768,                       
    antialias: true, 
    transparent: false, 
    resolution: 1
};

let assets = [
    "assets/sprites/block.png",
    "assets/sprites/drawcraft_logo.png",
    "assets/images/mainmenu-bg.png"
];

let game = new DrawcraftGame(pixiConfig, assets);
document.body.appendChild(game.view);
game.start();
