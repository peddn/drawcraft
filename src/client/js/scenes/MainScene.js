import Matter from 'matter-js';
import GameScene from '../lib/GameScene.js';
import GameBody from '../lib/GameBody.js';

class MainScene extends GameScene {
    constructor(name, game) {
        super(name, game);
        this.block1 = new GameBody('assets/sprites/block.png', Matter.Vector.create(512, 100), { isStatic: false }, this.game);
        this.addChild(this.block1);
        this.block2 = new GameBody('assets/sprites/block.png', Matter.Vector.create(480, 600), { isStatic: true }, this.game);
        this.addChild(this.block2);
    }

}

export default MainScene;