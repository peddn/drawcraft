import Matter from 'matter-js';
import GameScene from '../lib/GameScene.js';
import GameSprite from '../lib/GameSprite.js';

class MainMenuScene extends GameScene {
    constructor(name, game) {
        super(name, game);
        this.bgImage = new GameSprite('assets/images/mainmenu-bg.png', Matter.Vector.create(512, 384), this.game);
        this.addChild(this.bgImage);
        this.logo = new GameSprite('assets/sprites/drawcraft_logo.png', Matter.Vector.create(512, 100), this.game);
        this.addChild(this.logo);
        this.button = new GameSprite('assets/sprites/block.png', Matter.Vector.create(512, 400), this.game);
        this.button.on('click', this.clickHandler, this);
        this.button.interactive = true;
        this.addChild(this.button);
        this.reset();
    }

    clickHandler(event) {
        console.log('clicked');
        let mainScene = this.game.getScene('scene-main');
        if(mainScene.active) {
            this.game.deactivateScene('scene-main');
            //mainScene.reset();
        } else {
            this.game.activateScene('scene-main');
        }
        //this.game.resetScene('scene-main');
        //this.game.activateScene('scene-main');
        event.stopPropagation();
    }
}

export default MainMenuScene;