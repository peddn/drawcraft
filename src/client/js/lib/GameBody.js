import * as PIXI from 'pixi.js';
import Matter from 'matter-js';

class GameBody extends PIXI.Sprite {
    constructor(path, spawnPos, options, game) {
        super(game.resources[path].texture);
        this.spawnPos = spawnPos;
        this.options = options;
        this.game = game;
        this.body = undefined;
        this.reset();
    }

    reset() {
        this.anchor.x = 0.5;
        this.anchor.y = 0.5;
        this.x = this.spawnPos.x;
        this.y = this.spawnPos.y;
        if(this.body !== undefined) {
            Matter.Composite.remove(this.game.world, this.body);
        }
        this.body = Matter.Bodies.rectangle(this.spawnPos.x, this.spawnPos.y, this.width, this.height, this.options);
        Matter.World.add(this.game.world, this.body);
    }

    activate() {
        Matter.Sleeping.set(this.body, false);
        this.interactive = false;
    }

    deactivate() {
        Matter.Sleeping.set(this.body, true);
        this.interactive = false;
    }

    show() {
        this.visible = true;
    }

    hide() {
        this.visible = false;
    }

    destroy() {
        Matter.Composite.remove(this.game.world, this.body);
        this.destroy();
    }

    update() {
        this.x = this.body.position.x;
        this.y = this.body.position.y;
        this.rotation = this.body.angle;
    }
}

export default GameBody;