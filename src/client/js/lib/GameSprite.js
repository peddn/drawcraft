import * as PIXI from 'pixi.js';
import Matter from 'matter-js';

class GameBody extends PIXI.Sprite {
    constructor(path, spawnPos, game) {
        super(game.resources[path].texture);
        this.spawnPos = spawnPos;
        this.game = game;
        this.reset();
    }

    reset() {
        this.x = this.spawnPos.x;
        this.y = this.spawnPos.y;
        this.anchor.x = 0.5;
        this.anchor.y = 0.5;
    }

    activate() {
        this.interactive = false;
    }

    deactivate() {
        this.interactive = false;
    }

    show() {
        this.visible = true;
    }

    hide() {
        this.visible = false;
    }

    destroy() {
        this.destroy();
    }

    update() {
    }
}

export default GameBody;