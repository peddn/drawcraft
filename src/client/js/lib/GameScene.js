import * as PIXI from 'pixi.js';
import Matter from 'matter-js';

class GameScene extends PIXI.Container {
    constructor(name, game) {
        super();
        this.name = name;
        this.game = game;
        this.reset();
    }

    reset() {
        this.active = true;
        this.interactive = true;
        this.visible = true;
        for(let gameObject of this.children) {
            gameObject.reset();
        }
    }

    activate() {
        this.active = true;
        this.interactive = true;
        for(let gameObject of this.children) {
            gameObject.activate();
        }
    }

    deactivate() {
        this.active = false;
        this.interactive = false;
        for(let gameObject of this.children) {
            gameObject.deactivate();
        }
    }

    show() {
        this.visible = true;
    }

    hide() {
        this.visible = false;
    }

    destroy() {
        this.destroy({
            children: true
        });
    }

    update() {
        for(let gameObject of this.children) {
            gameObject.update();
        }
    }
    
}

export default GameScene;