import * as PIXI from 'pixi.js';
import Matter from 'matter-js';

class Game {

    constructor(pixiConfig, assets) {
        this.assets    = assets;
        this.engine    = Matter.Engine.create();
        this.world     = this.engine.world;
        this.app       = new PIXI.Application(pixiConfig);
        this.view      = this.app.view;
        this.stage     = this.app.stage;
        this.loader    = this.app.loader;
        this.resources = this.loader.resources;

        this.loader.on('progress', this.loadProgressHandler, this);
        this.loader.on('error', this.loadErrorHandler, this);
        this.loader.on('load', this.loadLoadHandler, this);
        this.loader.once('complete', this.loadCompleteHandler, this);
    }

    loadProgressHandler(loader, resource) {
        console.log("loading: " + resource.url + " " + loader.progress + "%"); 
    }

    loadErrorHandler(loader, resource) {
        console.log("error: ", resource); 
    }

    loadLoadHandler(loader, resource) {
    }

    loadCompleteHandler(loader, resource) {
    }

    loadResources(callback) {
        this.loader.add(this.assets);
        this.loader.load(callback);
    }

    setup() {
        console.log('setup');
    }

    start() {
        this.loadResources(() => {
            this.setup();
            this.app.ticker.add(delta => this.gameLoop(delta));
        })
    }

    gameLoop(delta) {
        Matter.Engine.update(this.engine, 1000 / 60);
        for(let scene of this.stage.children) {
            if(scene.active) {
                scene.update();
            }
        }
    }

    activateScene(name) {
        let scene = this.stage.getChildByName(name);
        scene.activate();
    }

    deactivateScene(name) {
        let scene = this.stage.getChildByName(name);
        scene.deactivate();
    }

    resetScene(name) {
        let scene = this.stage.getChildByName(name);
        scene.reset();
    }

    getScene(name) {
        return this.stage.getChildByName(name);
    }

}

export default Game;