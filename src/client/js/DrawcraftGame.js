import Game from './lib/Game.js';
import MainScene from './scenes/MainScene.js';
import MainMenuScene from './scenes/MainMenuScene.js';

class DrawcraftGame extends Game {
    constructor(pixiConfig, assets) {
        super(pixiConfig, assets);
    }

    setup() {
        let mainMenuScene = new MainMenuScene('scene-mainmenu', this);
        this.stage.addChild(mainMenuScene);
        let mainScene = new MainScene('scene-main', this);
        mainScene.deactivate();
        this.stage.addChild(mainScene);
    }
    
}

export default DrawcraftGame;